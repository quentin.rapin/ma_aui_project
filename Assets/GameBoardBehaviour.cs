using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoardBehaviour : MonoBehaviour
{
    //Logic representation of the gamegrid
    static char[] mGameGrid = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    //Enum to differantiate player from AI
    enum Player {Player_1, Player_AI};
    //To know whose turn it is
    static Player m_Player = Player.Player_1;

    //Cube prefab used by AI
    [SerializeField]
    GameObject cubePrefab;
    // Prefab of the win text
    [SerializeField]
    GameObject WinText;
    // Prefab of the lose text
    [SerializeField]
    GameObject LoseText;
    // Prefab of the draw text
    [SerializeField]
    GameObject DrawText;

    // Method to detect the collision between the cases and the player cubes
    void OnCollisionEnter(Collision collision)
    {
        // Detect collision only if it is the player that is placing a cube
        // Else we enter in an infinite loop where the AI places all the blocks
        if(collision.gameObject.tag == "PlayerPawn"){
            // Get collision point
            ContactPoint contact = collision.contacts[0];
            Vector3 position = contact.point;
            // Change the color of the colliding cube and freeze its position
            collision.gameObject.GetComponent<Renderer> ().material.color = Color.green;
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

            //Set the turn to player
            m_Player = Player.Player_1;

            int block_pos = -1;

            // Get the position of the player pawn by analyzing with case it collided with
            if(gameObject.name == "Pos1"){
                Debug.Log("Pos1");
                block_pos = 1;
            }else if(gameObject.name == "Pos2"){
                Debug.Log("Pos2");
                block_pos = 2;
            }else if(gameObject.name == "Pos3"){
                Debug.Log("Pos3");
                block_pos = 3;
            }else if(gameObject.name == "Pos4"){
                Debug.Log("Pos4");
                block_pos = 4;
            }else if(gameObject.name == "Pos5"){
                Debug.Log("Pos5");
                block_pos = 5;
            }else if(gameObject.name == "Pos6"){
                Debug.Log("Pos6");
                block_pos = 6;
            }else if(gameObject.name == "Pos7"){
                Debug.Log("Pos7");
                block_pos = 7;
            }else if(gameObject.name == "Pos8"){
                Debug.Log("Pos8");
                block_pos = 8;
            }else if(gameObject.name == "Pos9"){
                Debug.Log("Pos9");
                block_pos = 9;
            }

            Debug.Log("Player case: " + block_pos);

            // Check if the case is empty and add it to the logical game grid
            if (mGameGrid[block_pos] != 'X' && mGameGrid[block_pos] != 'O'){
                addFormtoCase(block_pos);
            }

            // Check if there is a win or a draw condition
            if(checkIfWin() == 0){
            }else if(checkIfWin() == 1){
                Debug.Log("Player won!");
                // Instantiate the win text over the middle case
                GameObject middle_case = GameObject.Find("Pos5");
                var win_offset = new Vector3(0, 1.0f, 0);
                var wintxt = Instantiate(WinText, (middle_case.transform.position + win_offset), Quaternion.identity);
                return;
            }else if(checkIfWin() == -1){
                Debug.Log("It's a draw!");
                // Instantiate the draw text over the middle case
                GameObject middle_case = GameObject.Find("Pos5");
                var win_offset = new Vector3(0, 1.0f, 0);
                var drawtxt = Instantiate(DrawText, (middle_case.transform.position + win_offset), Quaternion.identity);
                return;
            }

            Debug.Log("AI turn");
            m_Player = Player.Player_AI;

            // Randomizing the case until we get one that is free
            int pos = Random.Range(1,10);
            while (mGameGrid[pos]=='X' || mGameGrid[pos]=='O'){
                pos = Random.Range(1,10);
            }

            // Add AI move to the logical game grid
            if (mGameGrid[pos] != 'X' && mGameGrid[pos] != 'O'){
                addFormtoCase(pos);
            }

            Debug.Log("AI case: " + pos);

            //Instantiate the AI cube in red at the correct case
            GameObject ai_case = GameObject.Find("Pos" + pos);
            var offset = new Vector3(0, 0.7f, 0);
            var obj = Instantiate(cubePrefab, (ai_case.transform.position + offset), Quaternion.identity);
            obj.GetComponent<Renderer> ().material.color = Color.red;
            
            //Check if there is a lose or draw condition
            if(checkIfWin() == 0){
            }
            else if(checkIfWin() == 1){
                Debug.Log("AI won!");
                GameObject middle_case = GameObject.Find("Pos5");
                var win_offset = new Vector3(0, 1.0f, 0);
                var wintxt = Instantiate(LoseText, (middle_case.transform.position + win_offset), Quaternion.identity);
                return;
            }else if(checkIfWin() == -1){
                Debug.Log("It's a draw!");
                GameObject middle_case = GameObject.Find("Pos5");
                var win_offset = new Vector3(0, 1.0f, 0);
                var wintxt = Instantiate(DrawText, (middle_case.transform.position + win_offset), Quaternion.identity);
                return;
            }
        }
        
    }

    void addFormtoCase(int pos){
        if (m_Player == Player.Player_1){
            mGameGrid[pos] = 'X';
        }
        else {
            mGameGrid[pos] = 'O';
        }
    }

    // Start is called before the first frame update
    void Start()
    {
       GameObject plate = GameObject.Find("Cube");
       plate.GetComponent<Renderer>().material.color = Color.black;
       GameObject gameboard = GameObject.Find("game-board");
       gameboard.GetComponent<Renderer>().material.color = Color.yellow;      
    }

    int checkIfWin(){
        //Winning Condition For First Row
        if (mGameGrid[1] == mGameGrid[2] && mGameGrid[2] == mGameGrid[3])
        {
            return 1;
        }
        //Winning Condition For Second Row
        else if (mGameGrid[4] == mGameGrid[5] && mGameGrid[5] == mGameGrid[6])
        {
            return 1;
        }
        //Winning Condition For Third Row
        else if (mGameGrid[6] == mGameGrid[7] && mGameGrid[7] == mGameGrid[8])
        {
            return 1;
        }

        //Winning Condition For First Column
        else if (mGameGrid[1] == mGameGrid[4] && mGameGrid[4] == mGameGrid[7])
        {
            return 1;
        }
        //Winning Condition For Second Column
        else if (mGameGrid[2] == mGameGrid[5] && mGameGrid[5] == mGameGrid[8])
        {
            return 1;
        }
        //Winning Condition For Third Column
        else if (mGameGrid[3] == mGameGrid[6] && mGameGrid[6] == mGameGrid[9])
        {
            return 1;
        }

        else if (mGameGrid[1] == mGameGrid[5] && mGameGrid[5] == mGameGrid[9])
        {
            return 1;
        }
        else if (mGameGrid[3] == mGameGrid[5] && mGameGrid[5] == mGameGrid[7])
        {
            return 1;
        }

        // If all the cells or values filled with X or O then any player has won the match
        else if (mGameGrid[1] != '1' && mGameGrid[2] != '2' && mGameGrid[3] != '3' && 
                 mGameGrid[4] != '4' && mGameGrid[5] != '5' && mGameGrid[6] != '6' && 
                 mGameGrid[7] != '7' && mGameGrid[8] != '8' && mGameGrid[9] != '9')
        {
            return -1;
        }

        else
        {
            return 0;
        }
    }
}