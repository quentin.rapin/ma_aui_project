using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR.ARFoundation;

public class SpawnableManager : MonoBehaviour
{

    [SerializeField]
    ARRaycastManager m_RaycastManager;
    List<ARRaycastHit> m_Hits = new List<ARRaycastHit>();
    //Prefab to instantiate player cube
    [SerializeField]
    GameObject cubePrefab;
    //Prefab of our gameboard
    [SerializeField]
    GameObject gameBoardPrefab;

    //Variable to retain that we have instantiated a gameboard
    GameObject gameBoard = null;
    //To get the plane manager and disable it (did not work)
    public ARPlaneManager m_ARPlaneManager;

    //To get the AR Cam
    Camera arCam;
    //Used to spawn our object
    GameObject spawnedObject;


    // Start is called before the first frame update
    void Start()
    {
        spawnedObject = null;
        arCam = GameObject.Find("AR Camera").GetComponent<Camera>();
    }

    //Update is called once per frame
    void Update()
    {
    
        if (Input.touchCount == 0)
        return;

        RaycastHit hit;
        Ray ray = arCam.ScreenPointToRay(Input.GetTouch(0).position);

        if(m_RaycastManager.Raycast(Input.GetTouch(0).position, m_Hits))
        {
            if(Input.GetTouch(0).phase == TouchPhase.Began && spawnedObject == null)
            {
                if (Physics.Raycast(ray, out hit)) 
                {
                    if (hit.collider.gameObject.tag == "Spawnable")
                    {
                        spawnedObject = hit.collider.gameObject;
                    }
                    else
                    {
                        SpawnPrefab(m_Hits[0].pose.position);
                    }
                }
            } 
            else if(Input.GetTouch(0).phase == TouchPhase.Moved && spawnedObject != null)
            {
                spawnedObject.transform.position = m_Hits[0].pose.position;
            }
            if(Input.GetTouch(0).phase == TouchPhase.Ended) 
            {
                spawnedObject = null;
            }
        }
    }

    void Awake()
    {
        // Did not work, led to a null pointer reference
        m_ARPlaneManager = GetComponent<ARPlaneManager>();
    }

    private void SpawnPrefab(Vector3 spawnPosition)
    {
        //Check if a gameboard was already instantiated
        if(gameBoard == null)
        {
            //if not instantiate it
            gameBoard = Instantiate(gameBoardPrefab, (spawnPosition), Quaternion.identity);
            //Disabling the plane manager did not work
            //m_ARPlaneManager.enabled = !m_ARPlaneManager.enabled;
            return;
        } else {
            //Adding a little offset so the cube fall
            var offset = new Vector3(0, 0.7f, 0);
            //Instantiate the cube
            var obj = Instantiate(cubePrefab, (spawnPosition + offset), Quaternion.identity);
        }
    }
}